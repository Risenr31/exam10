﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using ТпЛр2.Models;
using Products.Kontroller;

namespace Products
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        int[] polesnost;
        int[] zena;
        public List<Ware> Pokupki = new List<Ware>();
        public List<int> BestPokupki = new List<int>();
        public List<Ware> MCen = new List<Ware>();

        public void ChooseFileClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog opfidi = new OpenFileDialog();
                opfidi.ShowDialog();
                List<string> Einkaufsliste = new List<string>();
                string productString;
                if (opfidi.FileName != "")
                {
                    productString = File.ReadAllText(opfidi.FileName);
                    Einkaufsliste.AddRange(productString.Split('\n'));
                }
                polesnost = new int[Einkaufsliste.Count];
                zena = new int[Einkaufsliste.Count];
                Pokupki.Clear();
                Menu.ItemsSource = null;
                for (int i = 0; i < Einkaufsliste.Count; i++)
                {
                    string[] odd = new string[4];
                    odd = Einkaufsliste[i].Split(':');
                    Pokupki.Add(item: new Artikel() { kategorie = odd[0], name = odd[1], preis = Convert.ToInt32(odd[2]), nutzbarkeit = Convert.ToInt32(odd[3]) });
                    zena[i] = Convert.ToInt32(odd[2]);
                    polesnost[i] = Convert.ToInt32(odd[3]);
                }
                Menu.ItemsSource = Pokupki;
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте формат файла");
            }
        }

        public void Count_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MinCen.Text == "")  //Если покупатель не ввёл минимальную полезность
                {
                    int i = 0;
                    Knapsack.Items.Clear();
                    int money = Math.Abs(Convert.ToInt32(Summ.Text));
                    BestPokupki = Knapsack_problem.knapsack(zena, polesnost, money);
                    for (i = 0; i < (BestPokupki.Count - 1); i++)
                        Knapsack.Items.Add(Pokupki[BestPokupki[i]].name);
                    Knapsack.Items.Add("Итоговая полезность: " + BestPokupki[i]);
                }
                else
                {
                    int i;
                    MCen.Clear();
                    Knapsack.Items.Clear();
                    int money = Math.Abs(Convert.ToInt32(Summ.Text)), mincen = Math.Abs(Convert.ToInt32(MinCen.Text));
                    for (i = 0; i < Pokupki.Count; i++)
                        if (Pokupki[i].nutzbarkeit >= mincen)
                            MCen.Add(Pokupki[i]);
                    int[] Mpolesnost = new int[MCen.Count];
                    int[] Mzena = new int[MCen.Count];
                    for (i = 0; i<MCen.Count; i++)
                    {
                        Mpolesnost[i] = MCen[i].nutzbarkeit;
                        Mzena[i] = MCen[i].preis;
                    }    
                    BestPokupki = Knapsack_problem.knapsack(Mzena, Mpolesnost, money);
                    for (i = 0; i < (BestPokupki.Count - 1); i++)
                        Knapsack.Items.Add(MCen[BestPokupki[i]].name);
                    Knapsack.Items.Add("Итоговая полезность: " + BestPokupki[i]);
                }
            }
            catch (Exception)
            { 
                MessageBox.Show("Введите корректно сумму денег и/или минимальную ценность");
                MinCen.Text = Summ.Text = "";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((Kateg.Text == "") || (Nutz.Text == "")||(Prc.Text == "")||(Prod.Text == ""))
                    MessageBox.Show("Проверьте правильность заполнения");
                else
                {
                    int a = Convert.ToInt32(Prc.Text), b = Convert.ToInt32(Nutz.Text);
                    FileStream File = new FileStream("Продукты.txt", FileMode.Append, FileAccess.Write);
                    StreamWriter Writer = new StreamWriter(File);
                    Writer.WriteLine();
                    Writer.Write(Kateg.Text+":"+ Prod.Text + ":" + Prc.Text + ":" + Nutz.Text);
                    Writer.Close();
                    File.Close();
                    Kateg.Text = Nutz.Text = Prc.Text = Prod.Text = "";
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте правильность заполнения");
            }
        }
    }
}
